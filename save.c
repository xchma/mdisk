#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <hdf5.h>
#include <hdf5_hl.h>

#include "nrsrc/nr.h"
#include "nrsrc/nrutil.h"
#include "prototypes.h"
#include "globvars.h"


void read_param(char *fname)
{
#define FLOAT 1
#define STRING 2
#define INT 3
#define MAXTAGS 300
    
    FILE *fd;
    char buf[200], buf1[200], buf2[200], buf3[200];
    int i, j, nt;
    int id[MAXTAGS];
    void *addr[MAXTAGS];
    char tag[MAXTAGS][50];
    int errorFlag = 0;

    nt = 0;
    
    /* halo parameters */
    strcpy(tag[nt], "CC");
    addr[nt] = &CC;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "Mvir");
    addr[nt] = &Mvir;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "LAMBDA");
    addr[nt] = &LAMBDA;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "HUBBLE");
    addr[nt] = &HUBBLE;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "Omega_0");
    addr[nt] = &Omega_0;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "Omega_Lambda");
    addr[nt] = &Omega_Lambda;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "Z");
    addr[nt] = &Z;
    id[nt++] = FLOAT;
      
    /* mass of different components */
    strcpy(tag[nt], "M_HALO");
    addr[nt] = &M_HALO;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "M_DISK");
    addr[nt] = &M_DISK;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "M_GAS");
    addr[nt] = &M_GAS;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "M_GASHALO");
    addr[nt] = &M_GASHALO;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "M_BULGE");
    addr[nt] = &M_BULGE;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "M_BH");
    addr[nt] = &M_BH;
    id[nt++] = FLOAT;
      
    /* particle number of different components */
    strcpy(tag[nt], "N_HALO");
    addr[nt] = &N_HALO;
    id[nt++] = INT;

    strcpy(tag[nt], "N_DISK");
    addr[nt] = &N_DISK;
    id[nt++] = INT;

    strcpy(tag[nt], "N_GAS");
    addr[nt] = &N_GAS;
    id[nt++] = INT;

    strcpy(tag[nt], "N_GASHALO");
    addr[nt] = &N_GASHALO;
    id[nt++] = INT;

    strcpy(tag[nt], "N_BULGE");
    addr[nt] = &N_BULGE;
    id[nt++] = INT;
      
    strcpy(tag[nt], "N_BH");
    addr[nt] = &N_BH;
    id[nt++] = INT;

    /* disk paramters */
    strcpy(tag[nt], "DARKMASS_IN_ROPT");
    addr[nt] = &DARKMASS_IN_ROPT;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "H");
    addr[nt] = &H;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "DiskHeight");
    addr[nt] = &DiskHeight;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "GasDistribution");
    addr[nt] = &GasDistribution;
    id[nt++] = INT;
      
    strcpy(tag[nt], "GasExpAlpha");
    addr[nt] = &GasExpAlpha;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "PowerLawGamma");
    addr[nt] = &PowerLawGamma;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "PowerLawCutOff");
    addr[nt] = &PowerLawCutOff;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "HoleRadius");
    addr[nt] = &HoleRadius;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "HoleGamma");
    addr[nt] = &HoleGamma;
    id[nt++] = FLOAT;
      
    /* bulge parameters */
    strcpy(tag[nt], "BulgeSize");
    addr[nt] = &BulgeSize;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "BulgeDistribution");
    addr[nt] = &BulgeDistribution;
    id[nt++] = INT;
      
    /* gas halo parameters */
    strcpy(tag[nt], "GasHalo_Rc_over_Rs");
    addr[nt] = &GasHalo_Rc_over_Rs;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "GasHalo_Beta");
    addr[nt] = &GasHalo_Beta;
    id[nt++] = FLOAT;
      
    /* don't know what this is (A) */
    strcpy(tag[nt], "SetInitModeAmp");
    addr[nt] = &SetInitModeAmp;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "SetInitModeCut");
    addr[nt] = &SetInitModeCut;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "SetInitModeM");
    addr[nt] = &SetInitModeM;
    id[nt++] = INT;
      
    /* don't know what this is (B) */
    strcpy(tag[nt], "HI_GasMassFraction");
    addr[nt] = &HI_GasMassFraction;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "HI_GasDiskScaleLength");
    addr[nt] = &HI_GasDiskScaleLength;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "Qstabilizefactor");
    addr[nt] = &Qstabilizefactor;
    id[nt++] = FLOAT;

    /* cold flow parameters */
    strcpy(tag[nt], "ColdFlowExtraMgas");
    addr[nt] = &ColdFlowExtraMgas;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "ColdFlowMdot");
    addr[nt] = &ColdFlowMdot;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "ColdFlowTheta");
    addr[nt] = &ColdFlowTheta;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "ColdFlowPhi");
    addr[nt] = &ColdFlowPhi;
    id[nt++] = FLOAT;
      
    strcpy(tag[nt], "ColdFlowRinner");
    addr[nt] = &ColdFlowRinner;
    id[nt++] = FLOAT;

    strcpy(tag[nt], "ColdFlowRadius");
    addr[nt] = &ColdFlowRadius;
    id[nt++] = FLOAT;
      
    /* IO paramters */
    strcpy(tag[nt], "OutputDir");
    addr[nt] = OutputDir;
    id[nt++] = STRING;
      
    strcpy(tag[nt], "OutputFile");
    addr[nt] = OutputFile;
    id[nt++] = STRING;
    
    /* start reading */
    if ((fd=fopen(fname, "r")))
    {
        while (!feof(fd))
        {
            buf[0] = 0;
            fgets(buf, 200, fd);
            
            if (sscanf(buf,"%s%s%s",buf1,buf2,buf3)<2) continue;

            if (buf1[0]=='%') continue;
            
            for (i=0,j=-1;i<nt;i++)
                if (strcmp(buf1,tag[i])==0)
                { j=i; tag[i][0]=0; break; }

            if (j>=0)
            {
                switch (id[j]) {
                    case FLOAT:
                        *((double *)addr[j]) = atof(buf2);
                        break;
                    case STRING:
                        strcpy(addr[j], buf2);
                        break;
                    case INT:
                        *((int *)addr[j]) = atoi(buf2);
                        break;
                }
            } else {
              fprintf(stdout, "Error in file %s: Tag '%s' not allowed or multiple defined.\n", fname, buf1);
              errorFlag = 1;
            }
        }
        fclose(fd);
    } else {
        fprintf(stdout, "Parameter file %s not found.\n", fname);
        errorFlag = 1;
    }

    for (i=0; i<nt; i++) {
        if (*tag[i]) {
            fprintf(stdout, "Error. I miss a value for tag '%s' in parameter file '%s'.\n", tag[i], fname);
            errorFlag = 1;
        }
    }

    if (errorFlag) exit(1);
    
    // just to be safe
    if (M_HALO==0) N_HALO=0;
    if (M_GAS==0) N_GAS=0;
    if (M_DISK==0) N_DISK=0;
    if (M_BULGE==0) N_BULGE=0;
    if (M_GASHALO==0) N_GASHALO=0;
    if (M_BH==0) N_BH=0;

#undef FLOAT
#undef STRING
#undef INT
#undef MAXTAGS
}


void save_hdf5(char* fname)
{
    // open file
    hid_t file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    herr_t status;
    
    printf("saving initial conditions to file `%s'\n\n",fname);
    
    // header
    hid_t group_id = H5Gcreate(file_id, "/Header", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    
    float mass_table[6]={0.};
    int npart[6]={N_GAS+N_GASHALO,N_HALO,N_DISK,N_BULGE,0,N_BH};
    status = H5LTset_attribute_int(file_id, "/Header", "NumPart_ThisFile", npart, 6);
    status = H5LTset_attribute_int(file_id, "/Header", "NumPart_Total", npart, 6);
    status = H5LTset_attribute_float(file_id, "/Header", "MassTable", mass_table, 6);
    
    status = H5Gclose(group_id);
    
    // write particles
    int i,j; int* buf_int; float* buf_float;
    
    // gas particles
    if (N_GAS+N_GASHALO)
    {
        group_id = H5Gcreate(file_id, "/PartType0", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        
        // coordinates
        buf_float = malloc((N_GAS+N_GASHALO)*3*sizeof(float));
        for (i=1;i<=N_GAS;i++)
        {
            j = i-1;
            buf_float[3*j] = xp_gas[i];
            buf_float[3*j+1] = yp_gas[i];
            buf_float[3*j+2] = zp_gas[i];
        }
        for (i=1;i<=N_GASHALO;i++)
        {
            j = i-1+N_GAS;
            buf_float[3*j] = xp_gashalo[i];
            buf_float[3*j+1] = yp_gashalo[i];
            buf_float[3*j+2] = zp_gashalo[i];
        }
        hsize_t dims2[2] = {N_GAS+N_GASHALO,3};
        status = H5LTmake_dataset_float(group_id, "Coordinates", 2, dims2, buf_float);
        free(buf_float);
        
        // velocities
        buf_float = malloc((N_GAS+N_GASHALO)*3*sizeof(float));
        for (i=1;i<=N_GAS;i++)
        {
            j = i-1;
            buf_float[3*j] = vxp_gas[i];
            buf_float[3*j+1] = vyp_gas[i];
            buf_float[3*j+2] = vzp_gas[i];
        }
        for (i=1;i<=N_GASHALO;i++)
        {
            j = i-1+N_GAS;
            buf_float[3*j] = vxp_gashalo[i];
            buf_float[3*j+1] = vyp_gashalo[i];
            buf_float[3*j+2] = vzp_gashalo[i];
        }
        status = H5LTmake_dataset_float(group_id, "Velocities", 2, dims2, buf_float);
        free(buf_float);
        
        // masses
        buf_float = malloc((N_GAS+N_GASHALO)*sizeof(float));
        for (i=1;i<=N_GAS;i++)
            buf_float[i-1] = mp_gas[i];
        for (i=1;i<=N_GASHALO;i++)
            buf_float[i-1+N_GAS] = mp_gashalo[i];
        hsize_t dims1[1] = {N_GAS+N_GASHALO};
        status = H5LTmake_dataset_float(group_id, "Masses", 1, dims1, buf_float);
        free(buf_float);
        
        // internal energy
        buf_float = malloc((N_GAS+N_GASHALO)*sizeof(float));
        for (i=1;i<=N_GAS;i++)
            buf_float[i-1] = u_gas[i];
        for (i=1;i<=N_GASHALO;i++)
            buf_float[i-1+N_GAS] = u_gashalo[i];
        status = H5LTmake_dataset_float(group_id, "InternalEnergy", 1, dims1, buf_float);
        free(buf_float);
        
        // particle IDs
        buf_int = malloc((N_GAS+N_GASHALO)*sizeof(int));
        for (i=1;i<=N_GAS;i++)
            buf_int[i-1] = i-1;
        for (i=1;i<=N_GASHALO;i++)
            buf_int[i-1+N_GAS] = i-1+N_GAS;
        status = H5LTmake_dataset_int(group_id, "ParticleIDs", 1, dims1, buf_int);
        free(buf_int);
        
        status = H5Gclose(group_id);
    }
    
    // halo particles
    if (N_HALO)
    {
        group_id = H5Gcreate(file_id, "/PartType1", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        
        // coordinates
        buf_float = malloc(N_HALO*3*sizeof(float));
        for (i=1;i<=N_HALO;i++)
        {
            j = i-1;
            buf_float[3*j] = xp_halo[i];
            buf_float[3*j+1] = yp_halo[i];
            buf_float[3*j+2] = zp_halo[i];
        }
        hsize_t dims2[2] = {N_HALO,3};
        status = H5LTmake_dataset_float(group_id, "Coordinates", 2, dims2, buf_float);
        free(buf_float);
        
        // velocities
        buf_float = malloc(N_HALO*3*sizeof(float));
        for (i=1;i<=N_HALO;i++)
        {
            j = i-1;
            buf_float[3*j] = vxp_halo[i];
            buf_float[3*j+1] = vyp_halo[i];
            buf_float[3*j+2] = vzp_halo[i];
        }
        status = H5LTmake_dataset_float(group_id, "Velocities", 2, dims2, buf_float);
        free(buf_float);
        
        // masses
        buf_float = malloc(N_HALO*sizeof(float));
        for (i=1;i<=N_HALO;i++)
            buf_float[i-1] = mp_halo[i];
        hsize_t dims1[1] = {N_HALO};
        status = H5LTmake_dataset_float(group_id, "Masses", 1, dims1, buf_float);
        free(buf_float);
        
        // particle IDs
        buf_int = malloc(N_HALO*sizeof(int));
        for (i=1;i<=N_HALO;i++)
            buf_int[i-1] = i-1+N_GAS+N_GASHALO;
        status = H5LTmake_dataset_int(group_id, "ParticleIDs", 1, dims1, buf_int);
        free(buf_int);
        
        status = H5Gclose(group_id);
    }
    
    // disk particles
    if (N_DISK)
    {
        group_id = H5Gcreate(file_id, "/PartType2", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        
        // coordinates
        buf_float = malloc(N_DISK*3*sizeof(float));
        for (i=1;i<=N_DISK;i++)
        {
            j = i-1;
            buf_float[3*j] = xp_disk[i];
            buf_float[3*j+1] = yp_disk[i];
            buf_float[3*j+2] = zp_disk[i];
        }
        hsize_t dims2[2] = {N_DISK,3};
        status = H5LTmake_dataset_float(group_id, "Coordinates", 2, dims2, buf_float);
        free(buf_float);
        
        // velocities
        buf_float = malloc(N_DISK*3*sizeof(float));
        for (i=1;i<=N_DISK;i++)
        {
            j = i-1;
            buf_float[3*j] = vxp_disk[i];
            buf_float[3*j+1] = vyp_disk[i];
            buf_float[3*j+2] = vzp_disk[i];
        }
        status = H5LTmake_dataset_float(group_id, "Velocities", 2, dims2, buf_float);
        free(buf_float);
        
        // masses
        buf_float = malloc(N_DISK*sizeof(float));
        for (i=1;i<=N_DISK;i++)
            buf_float[i-1] = mp_disk[i];
        hsize_t dims1[1] = {N_DISK};
        status = H5LTmake_dataset_float(group_id, "Masses", 1, dims1, buf_float);
        free(buf_float);
        
        // particle IDs
        buf_int = malloc(N_DISK*sizeof(int));
        for (i=1;i<=N_DISK;i++)
            buf_int[i-1] = i-1+N_GAS+N_GASHALO+N_HALO;
        status = H5LTmake_dataset_int(group_id, "ParticleIDs", 1, dims1, buf_int);
        free(buf_int);
        
        status = H5Gclose(group_id);
    }
    
    // bulge particles
    if (N_BULGE)
    {
        group_id = H5Gcreate(file_id, "/PartType3", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        
        // coordinates
        buf_float = malloc(N_BULGE*3*sizeof(float));
        for (i=1;i<=N_BULGE;i++)
        {
            j = i-1;
            buf_float[3*j] = xp_bulge[i];
            buf_float[3*j+1] = yp_bulge[i];
            buf_float[3*j+2] = zp_bulge[i];
        }
        hsize_t dims2[2] = {N_BULGE,3};
        status = H5LTmake_dataset_float(group_id, "Coordinates", 2, dims2, buf_float);
        free(buf_float);
        
        // velocities
        buf_float = malloc(N_BULGE*3*sizeof(float));
        for (i=1;i<=N_BULGE;i++)
        {
            j = i-1;
            buf_float[3*j] = vxp_bulge[i];
            buf_float[3*j+1] = vyp_bulge[i];
            buf_float[3*j+2] = vzp_bulge[i];
        }
        status = H5LTmake_dataset_float(group_id, "Velocities", 2, dims2, buf_float);
        free(buf_float);
        
        // masses
        buf_float = malloc(N_BULGE*sizeof(float));
        for (i=1;i<=N_BULGE;i++)
            buf_float[i-1] = mp_bulge[i];
        hsize_t dims1[1] = {N_BULGE};
        status = H5LTmake_dataset_float(group_id, "Masses", 1, dims1, buf_float);
        free(buf_float);
        
        // particle IDs
        buf_int = malloc(N_BULGE*sizeof(int));
        for (i=1;i<=N_BULGE;i++)
            buf_int[i-1] = i-1+N_GAS+N_GASHALO+N_HALO+N_DISK;
        status = H5LTmake_dataset_int(group_id, "ParticleIDs", 1, dims1, buf_int);
        free(buf_int);
        
        status = H5Gclose(group_id);
    }
    
    // BH particle
    if (N_BH)
    {
        group_id = H5Gcreate(file_id, "/PartType5", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        
        // coordinates
        buf_float = malloc(3*sizeof(float));
        for (i=1;i<=3;i++) buf_float[i-1] = 0.;
        hsize_t dims2[2] = {1,3};
        status = H5LTmake_dataset_float(group_id, "Coordinates", 2, dims2, buf_float);
        free(buf_float);
        
        // velocities
        buf_float = malloc(3*sizeof(float));
        for (i=1;i<=3;i++) buf_float[i-1] = 0.;
        status = H5LTmake_dataset_float(group_id, "Velocities", 2, dims2, buf_float);
        free(buf_float);
        
        // masses
        buf_float = malloc(1*sizeof(float));
        buf_float[0] = M_BH;
        hsize_t dims1[1] = {1};
        status = H5LTmake_dataset_float(group_id, "Masses", 1, dims1, buf_float);
        free(buf_float);
        
        // particle IDs
        buf_int = malloc(1*sizeof(int));
        buf_int[0] = N_GAS+N_GASHALO+N_HALO+N_DISK+N_BULGE;
        status = H5LTmake_dataset_int(group_id, "ParticleIDs", 1, dims1, buf_int);
        free(buf_int);
        
        status = H5Gclose(group_id);
    }
    
    // finished
    H5Fclose(file_id);
}


int write_header(FILE *fd)
{
    fprintf(fd,"GAL1    \n");
    fprintf(fd,"c       \t%g\n",CC);
    fprintf(fd,"mvir    \t%g\n",Mvir);
    fprintf(fd,"lambda  \t%g\n",LAMBDA);
    fprintf(fd,"mdisk   \t%g\n",M_DISK);
    fprintf(fd,"mbulge  \t%g\n",M_BULGE);
    fprintf(fd,"mgas    \t%g\n",M_GAS);
    fprintf(fd,"mbh     \t%g\n",M_BH);
    fprintf(fd,"disklen \t%g\n",H);
    fprintf(fd,"jd      \t%g\n",JD);
    fprintf(fd,"f_gas   \t%g\n",GasFraction);
    fprintf(fd,"disk_ht \t%g\n",DiskHeight);
    fprintf(fd,"bulge_a \t%g\n",BulgeSize);
    fprintf(fd,"n_halo  \t%d\n",N_HALO);
    fprintf(fd,"n_disk  \t%d\n",N_DISK);
    fprintf(fd,"n_gas   \t%d\n",N_GAS);
    fprintf(fd,"n_gashal\t%d\n",N_GASHALO);
    fprintf(fd,"n_bulge \t%d\n",N_BULGE);
    fprintf(fd,"hubble  \t%g\n",HUBBLE);
    fprintf(fd,"redshft \t%g\n",Z);
    fprintf(fd,"GasDist \t%d\n",GasDistribution);
    fprintf(fd,"GasExpA \t%g\n",GasExpAlpha);
    fprintf(fd,"PLGamma \t%g\n",PowerLawGamma);
    fprintf(fd,"PLCutO  \t%g\n",PowerLawCutOff);
    //fprintf(fd,"HoleR   \t%g\n",HoleRadius);
    //fprintf(fd,"HoleGam \t%g\n",HoleGamma);
    fprintf(fd,"BDist   \t%d\n",BulgeDistribution);
    //fprintf(fd,"m_iAmp  \t%g\n",SetInitModeAmp);
    //fprintf(fd,"m_Rcut  \t%g\n",SetInitModeCut);
    fprintf(fd,"Rvir    \t%g\n",Rvir);
    fprintf(fd,"Vvir    \t%g\n",Vvir);
    fprintf(fd,"Rd      \t%g\n",H);
    fprintf(fd,"Q       \t%g\n",Qstabilizefactor);
    fprintf(fd,"HI_gmf  \t%g\n",HI_GasMassFraction);
    fprintf(fd,"HI_gds  \t%g\n",HI_GasDiskScaleLength);
    fprintf(fd,"junk\n");
}
