#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "nrsrc/nr.h"
#include "nrsrc/nrutil.h"


#include "prototypes.h"
#include "globvars.h"

 
void compute_force_field()
{
    int i,j;
    printf("Start computing force field.\n");

    // R=0, z=0
    Dphi_R[0][0] = Dphi_z[0][0] = Dphi_z_dR[0][0] = 0;
    
    // R=0, z!=0
    for (j=1;j<=ZSIZE;j++)
    {
        Dphi_R[0][j] = 0;
        Dphi_z[0][j] = comp_Dphi_z(list_R[0], list_z[j]);
        Dphi_z_dR[0][j] = comp_Dphi_z(list_RplusdR[0], list_z[j]);
    }

    for (i=1;i<=RSIZE;i++)
    {
        printf("force field %d(%d)\n",i,RSIZE); fflush(stdout);

        for (j=0;j<=ZSIZE;j++)
        {
            if (j==0) { Dphi_z[i][j]=0; Dphi_z_dR[i][j]=0; } // z=0, R!=0
            else {
                Dphi_z[i][j]    = comp_Dphi_z(list_R[i], list_z[j]);
                Dphi_z_dR[i][j] = comp_Dphi_z(list_RplusdR[i], list_z[j]);
            }
            Dphi_R[i][j] = comp_Dphi_R( list_R[i], list_z[j]);
        }
    }

    double k2,Dphi_R_dR;
    for (i=1,epi_gamma2[0]=1;i<=RSIZE;i++)
    {
        Dphi_R_dR = comp_Dphi_R(list_RplusdR[i],0);
        k2 = 3 / list_R[i] * Dphi_R[i][0] + (Dphi_R_dR-Dphi_R[i][0]) / (list_RplusdR[i]-list_R[i]);
        epi_gamma2[i] = 4/list_R[i]*Dphi_R[i][0]/k2; // Springel+05, Eqn 19
        epi_kappa2[i] = k2;
    }
    epi_kappa2[0] = epi_kappa2[1];

    printf("Force field finished.\n");
}


double comp_Dphi_z(double R,double z)
{
  return comp_Dphi_z_halo(fabs(R),fabs(z))
        +comp_Dphi_z_bulge(fabs(R),fabs(z)) 
        +comp_Dphi_z_disk(fabs(R),fabs(z))
        +comp_Dphi_z_BH(fabs(R),fabs(z))
	    +comp_Dphi_z_gas(fabs(R),fabs(z))
	    +comp_Dphi_z_gashalo(fabs(R),fabs(z));
}


double comp_Dphi_R(double R,double z)
{
  return comp_Dphi_R_halo(fabs(R),fabs(z))
        +comp_Dphi_R_bulge(fabs(R),fabs(z)) 
        +comp_Dphi_R_disk(fabs(R),fabs(z))
        +comp_Dphi_R_BH(fabs(R),fabs(z))
	    +comp_Dphi_R_gas(fabs(R),fabs(z))
	    +comp_Dphi_R_gashalo(fabs(R),fabs(z));
}
