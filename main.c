#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "nrsrc/nr.h"
#include "nrsrc/nrutil.h"
#include "globvars.h"
#include "prototypes.h"


int main(int argc, char *argv[])
{
    /* read parameter file */
    if (argc!=2)
    {
        fprintf(stderr,"\n\nwrong argument(s).  Specify a parameter file.\n\n");
        exit(0);
    }
    
    char fparam[200];
    strcpy(fparam, argv[1]);
    read_param(fparam);
    mkdir(OutputDir, 02755);
    
    /* aux parameters */
    MD = (M_DISK+M_GAS)/Mvir;
    MB = M_BULGE/Mvir;
    GasFraction = M_GAS/(M_DISK+M_GAS);

    N_GAS_DISK = N_GAS;
    N_GAS_FLOW = (int)(ColdFlowExtraMgas/M_GAS)*N_GAS_DISK;
    N_GAS = N_GAS_DISK + N_GAS_FLOW;
    
    init_units();        /* set system of units */
    structure();         /* determine structure of halo, disk, and bulge */
    init();              /* allocate arrays */
    
    set_halo_positions();
    set_disk_positions();
    set_bulge_positions();
    set_gas_positions();
    set_gashalo_positions();
  
    compute_force_field();

    compute_velocity_dispersions_halo();
    compute_velocity_dispersions_disk();
    compute_velocity_dispersions_bulge();
    compute_velocity_dispersions_gas();
    compute_velocity_dispersions_gashalo();
 
    compute_local_escape_speed();

    set_halo_velocities();
    set_disk_velocities();
    set_bulge_velocities();
    set_gas_velocities();
    set_gashalo_velocities();

    // save IC to an hdf5 file
    char filename[100];
    sprintf(filename, "%s/%s", OutputDir, OutputFile);
    save_hdf5(filename);
    
    // useless below
    FILE* fd;
    strcat(filename,".parameters");
    if (fd=fopen(filename,"w"))
    {
        printf("writing circular velocity curve + Toomre's Q\n");
        write_header(fd);
        plot_circular_speeds(fd);
        plot_toomre_stability(fd);
        fclose(fd);
    } else {
        fprintf(stderr,"Can't open file '%s'.\n",filename);
        exit(0);
    }
    printf("done.\n");
    printf("Disk scale length: %g\n",H);
    printf("Rvir: %g\n",Rvir);
}
