#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "nrsrc/nr.h"
#include "nrsrc/nrutil.h"
#include "prototypes.h"
#include "globvars.h"


static double R,z;


void set_disk_velocities(void)
{
    int i,iz,ir;
    double q,R,phi,theta;
    long dum;
    double ur,uz;
    double vdisp_rz,vdisp_phi,vstream_phi;
    double vr,vphi;
    double vx,vy,vz;

    if (N_DISK==0) return;

    dum = drand48()*1e8;

    printf("set disk velocities..."); fflush(stdout);
  
    for (i=1;i<=N_DISK;i++)
    {
        R = sqrt(xp_disk[i]*xp_disk[i]+yp_disk[i]*yp_disk[i]);
        z = zp_disk[i];

        ir = (int)(log(R/LL*(pow(FR,RSIZE)-1)+1)/log(FR));
        ur = (log(R/LL*(pow(FR,RSIZE)-1)+1)/log(FR)) - ir;

        iz = (int)( log(fabs(z)/LL*(pow(FZ,ZSIZE)-1)+1)/log(FZ));
        uz = (log(fabs(z)/LL*(pow(FZ,ZSIZE)-1)+1)/log(FZ)) - iz;
        
        vdisp_rz = VelDispRz_disk[ir][iz]*(1-ur)*(1-uz)
                + VelDispRz_disk[ir+1][iz]*(ur)*(1-uz)
                + VelDispRz_disk[ir][iz+1]*(1-ur)*(uz)
                + VelDispRz_disk[ir+1][iz+1]*(ur)*(uz);

        vdisp_phi = VelDispPhi_disk[ir][iz]*(1-ur)*(1-uz)
                + VelDispPhi_disk[ir+1][iz]*(ur)*(1-uz)
                + VelDispPhi_disk[ir][iz+1]*(1-ur)*(uz)
                + VelDispPhi_disk[ir+1][iz+1]*(ur)*(uz);

        vstream_phi = VelStreamPhi_disk[ir][iz]*(1-ur)*(1-uz)
                + VelStreamPhi_disk[ir+1][iz]*(ur)*(1-uz)
                + VelStreamPhi_disk[ir][iz+1]*(1-ur)*(uz)
                + VelStreamPhi_disk[ir+1][iz+1]*(ur)*(uz);

        if (vdisp_rz<0)
        {
            printf("in disk: vdisp_rz: %g %g %g %d %d \n",vdisp_rz,ur,uz,ir,iz);
            vdisp_rz = -vdisp_rz;
        }
        if (vdisp_phi<0)
        {
            printf("in disk: vdisp_phi: %g %g %g %d %d\n",vdisp_phi,ur,uz,ir,iz);
            vdisp_phi = -vdisp_phi;
        }

        vr = gasdev(&dum)*sqrt(vdisp_rz)*Qstabilizefactor;
        vz = gasdev(&dum)*sqrt(vdisp_rz);
        vphi = vstream_phi+gasdev(&dum)*sqrt(vdisp_phi);

        vx = vr*xp_disk[i]/R - vphi*yp_disk[i]/R;
        vy = vr*yp_disk[i]/R + vphi*xp_disk[i]/R;

        vxp_disk[i] = vx;
        vyp_disk[i] = vy;
        vzp_disk[i] = vz;

        double vm2 = 2*vc2_sph_function(sqrt(R*R+z*z));
        if (vmax2_disk[i]>vm2) vm2=vmax2_disk[i];
        if ((vx*vx+vy*vy+vz*vz)>0.95*vm2) i--; // reject
    }
    printf("done.\n"); fflush(stdout);
}


void set_disk_positions(void)
{
    int i;
    double q,R,f,f_,Rold,phi,rtmp;

    if (N_DISK==0) return;
    
    srand48(DRAND);
  
    for (i=1;i<=N_DISK;)
    {
        rtmp = 10*LL;
        do
        {
            q = drand48();
            zp_disk[i] = (Z0/2)*log(q/(1-q)); // z coordinate
      
            q = drand48();
            R = 1.0;
            do // solving q == 1 - (1+x)*exp(-x)
            {
                f = (1+R)*exp(-R)+q-1;
                f_= -R*exp(-R);
                Rold = R;
                R = R-f/f_;
            }
            while(fabs(R-Rold)/R>1e-6);
            
            R *= H;
            rtmp = sqrt(R*R+zp_disk[i]*zp_disk[i]);
        }
        while (rtmp>=LL);
    
        phi = drand48()*PI*2;
        
        xp_disk[i] = R*cos(phi);
        yp_disk[i] = R*sin(phi);
        mp_disk[i] = M_DISK/N_DISK;
        
        i++;
    }
}
