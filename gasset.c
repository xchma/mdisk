#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "nrsrc/nr.h"
#include "nrsrc/nrutil.h"

#include "prototypes.h"
#include "globvars.h"


static double R,z;


void set_gas_velocities(void)
{
    int i,iz,ir;
    double q,R,phi,theta;
    long dum;
    double ur,uz;
    double vdisp_rz,vdisp_phi,vstream_phi;
    double vr,vphi;
    double vx,vy,vz;
    double vdum,tdum,vx0,vy0,vz0;
    double xt,yt,zt,rt;

    if (N_GAS==0) return;

    dum = drand48()*1e8;

    printf("set gas velocities..."); fflush(stdout);
  
    for(i=1;i<=N_GAS_DISK;i++)
    {
        R = sqrt(xp_gas[i]*xp_gas[i]+yp_gas[i]*yp_gas[i]);
        z = zp_gas[i];
        
        ir = (int)(log(R/LL*(pow(FR,RSIZE)-1)+1)/log(FR));
        ur = (log(R/LL*(pow(FR,RSIZE)-1)+1)/log(FR)) - ir;

        iz = (int)(log(fabs(z)/LL*(pow(FZ,ZSIZE)-1)+1)/log(FZ));
        uz = (log(fabs(z)/LL*(pow(FZ,ZSIZE)-1)+1)/log(FZ)) - iz;
        
        /* -------------------
	     * Set Gas Temperature
         --------------------- */
        vdisp_rz = VelDispRz_gas[ir][iz]*(1-ur)*(1-uz)
                + VelDispRz_gas[ir+1][iz]*(ur)*(1-uz)
                + VelDispRz_gas[ir][iz+1]*(1-ur)*(uz)
                + VelDispRz_gas[ir+1][iz+1]*(ur)*(uz);

        u_gas[i] = vdisp_rz/(GAMMA-1);

        if (u_gas[i]<=1.0) u_gas[i] = 1.0;

	 
        /*
         this assumes that the dispersion above is effective;
         but if its too large, it won't do anything, b/c it
         will immediately cool away, leaving the system without
         that pressure and so sub-Keplerian in velocity
            u_gas_max ~ 5.0d4 * Q_eos
         
         REAL solution if this is a worry is to enforce an
         IC with a razor-thin disk; i.e. already cooled
         */

        vstream_phi = VelStreamPhi_gas[ir][iz]*(1-ur)*(1-uz)
                + VelStreamPhi_gas[ir+1][iz]*(ur)*(1-uz)
                + VelStreamPhi_gas[ir][iz+1]*(1-ur)*(uz)
                + VelStreamPhi_gas[ir+1][iz+1]*(ur)*(uz);
        vr = 0;
        vz = 0;
        vphi = vstream_phi;

	
        /*
         use the below lines if we want to initialize the gas disk as
         a cold, dispersion-dominated disk (i.e. like a stellar disk),
         rather than as the normal thermally supported disk
      
         vdisp_phi = VelDispPhi_gas[ir][iz]*(1-ur)*(1-uz)
                + VelDispPhi_gas[ir+1][iz]*(ur)*(1-uz)
                + VelDispPhi_gas[ir][iz+1]*(1-ur)*(uz)
                + VelDispPhi_gas[ir+1][iz+1]*(ur)*(uz);
         if (vdisp_rz<0)  vdisp_rz = -vdisp_rz;
         if (vdisp_phi<0) vdisp_phi = -vdisp_phi;
         vr = gasdev(&dum)*sqrt(vdisp_rz)*Qstabilizefactor;
         vz = gasdev(&dum)*sqrt(vdisp_rz);
         vphi = vstream_phi + gasdev(&dum)*sqrt(vdisp_phi);
         u_gas[i] = vdisp_rz/(GAMMA-1);
         if(u_gas[i]>=30000) u_gas[i]=30000; // 30000 is T~3e6 K
         if(u_gas[i]>=300) u_gas[i]=300; // 300 is T~3e4 K
         if(u_gas[i]<=1.0) u_gas[i]=1.0; ; // 1 is T~100 K
         
         here endeth the part of the code added to make the gas vdisp supported
         */

        vx = vr*xp_gas[i]/R - vphi*yp_gas[i]/R;
        vy = vr*yp_gas[i]/R + vphi*xp_gas[i]/R;

        vxp_gas[i] = vx;
        vyp_gas[i] = vy;
        vzp_gas[i] = vz;
      
        double vm2 = 2*vc2_sph_function(sqrt(R*R+z*z));
        if (vmax2_gas[i]>vm2) vm2 = vmax2_gas[i];
        if ((vx*vx+vy*vy+vz*vz)>0.95*vm2) i--;
    }
      
    if (N_GAS_FLOW > 0)
    {
        for (i=N_GAS_DISK+1;i<=N_GAS;i++)
        {
            tdum = 0.017453; // =PI/180
            xt = xp_gas[i] - ColdFlowRinner;
            yt = yp_gas[i];
            zt = zp_gas[i];
            rt = sqrt(xt*xt+yt*yt+zt*zt);
            vz = Vvir;
            vx0 = -xt/rt * vz;
            vy0 = -yt/rt * vz;
            vz0 = -zt/rt * vz;
            vdum = gasdev(&dum);
            if (vdum>5.0) vdum = 5.0;
            if (vdum<-5.0) vdum = -5.0;
            vx0 = vx0*(1.0+0.1*vdum);
            vdum = gasdev(&dum);
            if (vdum>5.0) vdum = 5.0;
            if (vdum<-5.0) vdum = -5.0;
            vy0 = vy0*(1.0+0.1*vdum);
            vdum = gasdev(&dum);
            if (vdum>5.0) vdum = 5.0;
            if (vdum<-5.0) vdum = -5.0;
            vz0 = vz0*(1.0+0.1*vdum);
            vxp_gas[i] = vx0;
            vyp_gas[i] = vy0;
            vzp_gas[i] = vz0;
            u_gas[i] = 5000;
        }
    }
    printf("done.\n"); fflush(stdout);
}


void set_gas_positions(void)
{
    int i; double rtmp,Rd;
    double q,R,f,f_,Rold,phi,phiold,theta,F1_HOLE,F2_HOLE;
    double m_mode,eps_mode,epm1,epm2,epm3,epm4;

    if (N_GAS_DISK==0) return;

    srand48(GRAND);
  
    int n_disk,n_HI;
    if (HI_GasMassFraction>0)
    {
        n_disk = (1-HI_GasMassFraction)*N_GAS_DISK;
        n_HI = N_GAS_DISK - n_disk;
    }
    else
    {
        n_disk = N_GAS_DISK;
        n_HI = 0;
    }

    switch (GasDistribution) {
        case 0:
            Rd = H;
            break;
        case 1:
            Rd = H*GasExpAlpha;
            break;
        case 2:
            Rd = H;
            break;
        case 3:
            Rd = H*GasExpAlpha;
            F1_HOLE = (HoleRadius*HoleRadius) / (2+HoleGamma+HoleRadius*(2+HoleRadius+HoleGamma));
            F2_HOLE = exp(HoleRadius)*(2+HoleGamma) / (2+HoleGamma+HoleRadius*(2+HoleRadius+HoleGamma));
            break;
    }

    printf("Gas Rd=%g\n", Rd);
    
    for (i=1;i<=n_disk;)
    {
        rtmp = 10*LL;
        do {
            q = drand48();
            zp_gas[i] = (Z0/2)*log(q/(1-q));

            q = drand48();
      
            switch (GasDistribution) {
                case 0: // Exponential Distribution
                case 1:
                    R = 1.0;
                    do {
                        f = (1+R)*exp(-R)+q-1;
                        f_= -R*exp(-R);
                        Rold = R;
                        R = R-f/f_;
                    } while (fabs(R-Rold)/R>1e-6);
                    R*=Rd;
                    break;
                case 2: // Power-law distribution
                    R = pow(q,1/(2-PowerLawGamma));
                    R *= (H*PowerLawCutOff);
                    break;
                case 3:
                    if (q<F1_HOLE)
                        R = HoleRadius*pow(q/F1_HOLE,1./(2.+HoleGamma));
                    else
                    {
                        R = 1.0;
                        do {
                            f = (1+R)*exp(-R)*F2_HOLE+q-1;
                            f_= -R*exp(-R)*F2_HOLE;
                            Rold = R;
                            R = R-f/f_;
                        } while (fabs(R-Rold)/R>1e-6);
                    }
                    R *= Rd;
                    break;
            }
            rtmp = sqrt(R*R+zp_gas[i]*zp_gas[i]);
        } while (rtmp>=LL);
      
        phi = drand48()*PI*2;
        if (SetInitModeAmp>0)
        {
            q = drand48();
            phi = 2.*PI*q;
            m_mode = SetInitModeM;
            eps_mode = SetInitModeAmp*R/(SetInitModeCut+R);
            do {
                f = q - (phi/(2.*PI)+eps_mode*sin(m_mode*phi)/m_mode);
                f_= -(1./(2.*PI)+eps_mode*cos(m_mode*phi));

                phiold = phi;
                phi = phi-f/f_;
                phi = fmod(phi,2.*PI);
            } while (fabs((phi-phiold)/phi)>1e-3);
        }

        xp_gas[i] = R*cos(phi);
        yp_gas[i] = R*sin(phi);
        
        i++;
    }

    for (i=1+n_disk;i<=N_GAS_DISK;)
    {
        rtmp = 10*LL;
        do {
            q = drand48();
            zp_gas[i] = (Z0/2)*log(q/(1-q));

            q = drand48();
            R = H*sqrt(q)*HI_GasDiskScaleLength;
            
            rtmp = sqrt(R*R+zp_gas[i]*zp_gas[i]);
        } while (rtmp>=LL);
      
        phi = drand48()*PI*2;
        xp_gas[i] = R*cos(phi);
        yp_gas[i] = R*sin(phi);
        
        i++;
    }

    if (N_GAS_FLOW>0)
    {
        for (i=N_GAS_DISK+1;i<=N_GAS;i++)
        {
            Rd = 2.0*Vvir*ColdFlowExtraMgas/(0.1*ColdFlowMdot);
            q = drand48();
            zp_gas[i] = Rd*q + 0.5*0.5*4.8*ColdFlowRinner;
              
            phi = drand48()*PI*2.0;
            q = drand48();
            R = ColdFlowRadius * sqrt(q) * (zp_gas[i]/(4.8*ColdFlowRinner));
            xp_gas[i] = R*cos(phi);
            yp_gas[i] = R*sin(phi);
            
            epm4 = 0.017453;
            epm1 = xp_gas[i];
            epm2 = yp_gas[i];
            epm3 = zp_gas[i];
            xp_gas[i] = epm1;
            yp_gas[i] = cos(ColdFlowTheta*epm4)*epm2 + sin(ColdFlowTheta*epm4)*epm3;
            zp_gas[i] = -sin(ColdFlowTheta*epm4)*epm2 + cos(ColdFlowTheta*epm4)*epm3;
            
            epm1 =xp_gas[i];
            epm2 =yp_gas[i];
            epm3 =zp_gas[i];
            xp_gas[i] = cos(ColdFlowPhi*epm4)*epm1 + sin(ColdFlowPhi*epm4)*epm2;
            yp_gas[i] = -sin(ColdFlowPhi*epm4)*epm1 + cos(ColdFlowPhi*epm4)*epm2;
            zp_gas[i] = epm3;
             
            xp_gas[i] = xp_gas[i] + ColdFlowRinner;
        }
    }
    
    for (i=1;i<=N_GAS;i++)
        mp_gas[i] = M_GAS/N_GAS_DISK;
}
