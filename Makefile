EXEC   = mdisk

SRCS   = main.c  globvars.c \
	halo.c haloset.c escape.c \
	disk.c diskset.c \
	bulge.c bulgeset.c \
	gas.c  gasset.c \
	gashalo.c gashalofns.c gashaloset.c \
	save.c init.c structure.c \
	misc.c toomre.c force.c \
	forcetree.c \
	nrsrc/bessi0.c   nrsrc/bessj1.c   \
	nrsrc/erff.c     nrsrc/polint.c   nrsrc/spline.c \
	nrsrc/bessi1.c   nrsrc/bessk0.c   nrsrc/gasdev.c  \
	nrsrc/qromb.c    nrsrc/splint.c  \
	nrsrc/bessj0.c   nrsrc/bessk1.c   \
	nrsrc/nrutil.c   nrsrc/ran1.c     nrsrc/trapzd.c \
	nrsrc/gammp.c    nrsrc/gcf.c      nrsrc/gser.c  \
        nrsrc/gammln.c   nrsrc/indexx.c   nrsrc/zriddr.c

OBJS   = $(SRCS:.c=.o)
INCL   = prototypes.h globvars.h nrsrc/nrutil.h nrsrc/nr.h

HDF5_INC = $(HDF5_PATH)/include
HDF5_LIB = $(HDF5_PATH)/lib

CFLAGS = -fPIC -O3 -g -m64
CFLAGS += -Qunused-arguments

INCOPT = -I$(HDF5_INC)
LIBOPT = -L$(HDF5_LIB) -lhdf5 -lhdf5_hl

CC     =  gcc

$(EXEC): $(OBJS) 
	$(CC) $(CFLAGS) $(INCOPT) $(LIBOPT) $(OBJS) -o $(EXEC)

.c.o:
	$(CC) $(CFLAGS) $(INCOPT) $(LIBOPT) -c -o $@ $<

clean:
	rm -f $(OBJS) $(EXEC)
