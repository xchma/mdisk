#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "nrsrc/nr.h"
#include "nrsrc/nrutil.h"

#include "prototypes.h"
#include "globvars.h"




#define  N  100000   /* number of mass bins for halo */



/* a number of tables */

static double *rt,*mt,*m2r,*r2m;
static double *rfinal;  
static double *mfinal;
static double *rhofinal,*rho2r;  

static int III;   /* a dummy variable */



static double rvir_halo,a_halo,rho0_halo,rhot_halo,gam1_halo,p1_halo;



void structure()
{
    void prepare_cumlative_profile(void);
    double disk_angmomentum(void);
    double additional_mass_in_halo_cutoff(void);
    double darkmass_inside_ropt(void);
    double fc(double);
    double gc(double);
    int i,pass;
    double jhalo,jd;
    double dm=0;
    double x, Omega_z, Delta_vir;
    double mass_dm;

    /*  new method  */
    EZ = sqrt( Omega_Lambda +
               (1-Omega_0-Omega_Lambda) * (1+Z) * (1+Z) +
               Omega_0 * (1+Z) * (1+Z) * (1+Z) );
    printf("E(z)=%g\n", EZ);
    Omega_z = Omega_0 * (1+Z) * (1+Z) * (1+Z) / (EZ * EZ);
    printf("Omega_z=%g\n",Omega_z);
    x = Omega_z - 1.0;
    
    // Bryan & Normal (1998) virial parameter
    // w.r.t. critical density at redshift z
    Delta_vir = 18*PI*PI + 82*x - 39*x*x;
    printf("Delta_vir=%g\n", Delta_vir);
    
    printf("H=%g\n", H);
    Z0 = DiskHeight*H;   /* sets disk thickness */
    A = BulgeSize;       /* sets bulge size -- now an absolute size */
    
    pass = 0;
    
    if (DARKMASS_IN_ROPT<0.0)
    {
        /* setup NFW properties the old fashioned way -- no contraction */
        Mvir = Mvir + dm; //note that dm=0
        Rvir = pow(2*G*Mvir/(Delta_vir*H0*H0*EZ*EZ), 1.0/3.0);
        Vvir = sqrt(G*Mvir/Rvir);
        RS = Rvir/CC;
        M_TOTAL = M_HALO; // + additional_mass_in_halo_cutoff();
        rho0_halo = M_HALO / ( 4*PI*(log(1+CC)-CC/(1+CC)) * RS*RS*RS );
        //M_HALO=M_TOTAL;//-M_DISK-M_GAS-M_BULGE;
        jhalo = LAMBDA*sqrt(G)*pow(Mvir,1.5)*sqrt(2*Rvir/fc(CC));
        halo_spinfactor = 1.5*LAMBDA*sqrt(2*CC/fc(CC))*pow(log(1+CC)-CC/(1+CC),1.5)/gc(CC);
        MD = (M_DISK+M_GAS)/Mvir;
        MB = M_BULGE/Mvir; // redundant???
        setup_massprofile(0); // sets up NFW profile, with virial mass
        RHO_0 = M_GASHALO/(4*PI*qromb(halogas_profile_int,REPS,Rvir));
        setup_halogas_massprofile();
        mass_dm = darkmass_inside_ropt(); // dark matter mass inside optical radius
    } else {
        do
        {
            /* setup NFW properties */
            /* one actually solves for Mvir given a DARKMASS_IN_ROPT */
            Mvir=Mvir+Mvir*dm/M_HALO;
            Rvir=pow(2*G*Mvir/(Delta_vir*H0*H0*EZ*EZ),1.0/3.0);
            Vvir=sqrt(G*Mvir/Rvir);
            RS=Rvir/CC;
            M_TOTAL=M_HALO + additional_mass_in_halo_cutoff();
            rho0_halo=M_HALO/( 4*PI*(log(1+CC)-CC/(1+CC)) * RS*RS*RS);
            //M_HALO=M_TOTAL;//-M_DISK-M_GAS-M_BULGE;
            jhalo=LAMBDA*sqrt(G)*pow(Mvir,1.5)*sqrt(2*Rvir/fc(CC));
            halo_spinfactor=1.5*LAMBDA*sqrt(2*CC/fc(CC))*pow(log(1+CC)-CC/(1+CC),1.5)/gc(CC);
            MD= (M_DISK+M_GAS)/Mvir;
            MB= M_BULGE/Mvir;
            
            if(pass==0)
              {
                setup_massprofile(0);  /* sets up NFW profile, with virial mass */
                    RHO_0=M_GASHALO/(4*PI*qromb(halogas_profile_int,REPS,Rvir));
                setup_halogas_massprofile();
                pass= 1;
              }
            else
              {
                setup_massprofile(1);
                RHO_0=M_GASHALO/(4*PI*qromb(halogas_profile_int,REPS,Rvir));
                setup_halogas_massprofile();
              }

            solve_mass_shells();  /* adiabatic contraction */
            RHO_0=M_GASHALO/(4*PI*qromb(halogas_profile_int,REPS,Rvir));

            mass_dm= darkmass_inside_ropt();   /*  dark matter mass inside optical radius */

            dm= DARKMASS_IN_ROPT-mass_dm;

            if(fabs(dm)>0.1*DARKMASS_IN_ROPT)
              {
                dm= 5*dm;
              }
            else
              dm=dm*0.5;
     	}
        while(fabs(2*dm)>1e-3);
    }
    
    printf("\nRvir=%g\n", Rvir);
    printf("Vvir=%g\n", Vvir);
    printf("Mvir=%g (c=%g)\n\n", Mvir, CC);
    printf("Mhalo=%g\n", M_HALO);
    printf("Mdisk=%g\n", M_DISK);
    printf("Mgas=%g\n", M_GAS);
    printf("Mgashalo=%g\n", M_GASHALO);
    printf("Mbulge=%g\n\n", M_BULGE);
    printf("Dark Mass inside Ropt=%g (DM_IN_ROPT=%g)\n\n", mass_dm, DARKMASS_IN_ROPT);
    printf("MD=%g\n", MD);
    printf("MB=%g\n", MB);
    printf("rho_s=%g\n", rho0_halo);
    printf("rho_0=%g\n", RHO_0);
    printf("r_s=%g\n", RS);
    printf("E=%g\n", -G*M_TOTAL*M_TOTAL*fc(CC)/(2*Rvir));
    printf("J_halo=%g\n", jhalo);
    printf("Total mass will be: %g\n\n", M_TOTAL);
  
    jd = disk_angmomentum(); // computes disk momentum
    JD = jd/jhalo;
    printf("JD=Jdisk/Jhalo=%g\n\n", JD);
    
    prepare_cumlative_profile(); // prepare cumulative profile of dark mass
}



double disk_angmomentum(void)
{
    double jdisk_int(double);
    double jgas_int(double);
    double jbulge_int(double); // does not exist
    double dmin(double a,double b);
    double j_disk=0,j_gas=0,j_bulge=0;
    
    j_disk = qromb(jdisk_int, 0, dmin(30*H,Rvir));
    j_gas = qromb(jgas_int, 0, dmin(100*H,Rvir));

    printf("j_disk=%g\n", j_disk);
    printf("j_gas=%g\n", j_gas);
    printf("j_bulge=%g\n", j_bulge);
    
    return j_disk+j_gas+j_bulge;
}


double dmin(double a, double b)
{
    if(a<b)
        return a;
    else
        return b;
}




double jdisk_int(double x)
{
    double vc2,vc,y,jint,Rd;
  
    switch (GasDistribution)
    {
        case 0:
          Rd = H;
          break;
        case 1:
          Rd = H*GasExpAlpha;
          break;
        case 2:
          Rd = H;
          break;
        case 3:
            Rd = 1.0*H;
          break;
    }
  
    if (x<=1e-20)
        vc2 = 0;
    else
        vc2 = G * (mass_total_dark(x) + mass_cumulative_bulge(x) + mass_cumulative_gashalo(x) + M_BH) / x; // spherical component
    if (vc2<0) { vc2 = 0; printf("wwww1\n"); exit(0); }

    /*  -----------------------------------------
     *    Get Disk Contribution to Vc
     *  ----------------------------------------- */
    y = x / (2*H);
    if (y>1e-10) vc2 += 2*G*M_DISK/H*y*y*(bessi0(y)*bessk0(y)-bessi1(y)*bessk1(y));
    
    /*  -----------------------------------------
     *    Get Gas Disk Contribution to Vc
     *  ----------------------------------------- */
    y = x / (2*Rd); // not hold for non-exponential disks
    if (y>1e-10) vc2 += 2*G*M_GAS/H*y*y*(bessi0(y)*bessk0(y)-bessi1(y)*bessk1(y));

    vc = sqrt(vc2);
    
    // Springel+05, MN, 361, 776 (Eqn 8)
    jint = M_DISK*pow(x/H,2)*vc*exp(-x/H);

    return jint;
}


double vc2_sph_function(double r)
{
  double vc2;
  if(r<=0) return 0;
  vc2 = G * (mass_total_dark(r) + mass_cumulative_bulge(r) + mass_cumulative_disk(r) + 
        mass_cumulative_gas(r) + mass_cumulative_gashalo(r) + M_BH) /r;
  if (vc2<=0) return 0;
  return vc2;
}


double jgas_int(double x)
{
    double vc2,vc,y,jint,Rd;
    
    switch(GasDistribution) {
        case 0:
          Rd = H;
          break;
        case 1:
          Rd = H*GasExpAlpha;
          break;
        case 2:
          Rd = H;
          break;
        case 3:
          Rd = 1.0*H;
          break;
    }
    
    if (x<=1e-20)
        vc2 = 0;
    else
        vc2 = G * (mass_total_dark(x) + mass_cumulative_bulge(x) + mass_cumulative_gashalo(x) + M_BH) / x; // spherical component
    if (vc2<0) { vc2 = 0; printf("wwww1\n"); exit(0); }
    
    /*  -----------------------------------------
     *    Get Disk Contribution to Vc
     *  ----------------------------------------- */
    y = x / (2*H);
    if (y>1e-10) vc2 += 2*G*M_DISK/H*y*y*(bessi0(y)*bessk0(y)-bessi1(y)*bessk1(y));
    
    /*  -----------------------------------------
     *    Get Gas Disk Contribution to Vc
     *  ----------------------------------------- */
    y = x / (2*Rd); // not hold for non-exponential disks
    if (y>1e-10) vc2 += 2*G*M_GAS/H*y*y*(bessi0(y)*bessk0(y)-bessi1(y)*bessk1(y));
    
    vc = sqrt(vc2);
    
    // Springel+05, MN, 361, 776 (Eqn 8)
    jint = M_DISK*pow(x/H,2)*vc*exp(-x/H);

    return jint;
}



solve_mass_shells()
{
  int i;
  double zriddr(double (*func)(double), double x1, double x2, double xacc);
  double masszero(double);


  for(i=2;i<=(N-1);i++)
    {
      III=i;
      rfinal[i]=zriddr(masszero,0,rt[N],1e-6*rt[2]);
    }


  for(i=2,mfinal[1]=0;i<=N;i++)
    {
      mfinal[i]= rt[i]/rfinal[i]* mt[i]; //- mass_cumulative_disk(rfinal[i]) - mass_cumulative_bulge(rfinal[i]) - mass_cumulative_gas(rfinal[i]);
    }

  spline(rfinal,mfinal,N,1e40,1e40,m2r);
  spline(mfinal,rfinal,N,1e40,1e40,r2m);

}


double masszero(double rf)
{
  double mi,ri;
  double mass_cumulative_gas(double r);
  double mass_cumulative_gashalo(double r);
  double mass_cumulative_disk(double r);
  double mass_cumulative_bulge(double r);  
  
  mi=mt[III];
  ri=rt[III];

  return mi*ri-rf*( (1- Mvir/mt[N]*(MD+MB))*mi + mass_cumulative_disk(rf) + 
    mass_cumulative_bulge(rf) + mass_cumulative_gas(rf) + mass_cumulative_gashalo(rf));
}


double additional_mass_in_halo_cutoff(void)
{
  return 0;

  a_halo=CC-(1+3*CC)/(1+CC);
  rho0_halo=M_HALO/( 4*PI*(log(1+CC)-CC/(1+CC)) * RS*RS*RS);
  rhot_halo=rho0_halo/(CC*(1+CC)*(1+CC));
  gam1_halo=4*PI*rhot_halo*Rvir*Rvir*Rvir*exp(CC+gammln(3+a_halo)-(3+a_halo)*log(CC));
  p1_halo=gam1_halo*gammp(3+a_halo,CC);
  
  return gam1_halo-p1_halo;
}




/* MFW dark halo profile */
void setup_massprofile(int mode)
{
    int i,icount;
    double q,s,qq,f,f_,ds,r;
    
    if (mode==0)
    {
        rt = dvector(1,N);
        mt = dvector(1,N);
        mfinal = dvector(1,N); // redundant???
        r2m = dvector(1,N);
        m2r = dvector(1,N);
        rfinal = dvector(1,N); // redundant???
        rhofinal = dvector(1,N); // not used???
        rho2r = dvector(1,N); // not used???
    }
    
    if (mode==1)
    {
        for (i=1;i<=N;i++)
        {
            rt[i] = 0;
            mt[i] = 0;
            mfinal[i] = 0;
            r2m[i] = 0;
            m2r[i] = 0;
            rfinal[i] = 0;
            rhofinal[i] = 0;
            rho2r[i] = 0;
        }
    }
  
    // NFW mass profile: M(r) = M_vir f(cx)/f(c),
    // where f(c) := ln(1+c) - c / (1+c),
    // x := r / rs, rs := r_vir / c
    for (i=2,mt[1]=rt[1]=0;i<=N;i++)
    {
        mt[i] = (i-1.001)*M_TOTAL/(N-1); // why 1.001???
        q = mt[i]/M_TOTAL; // q := M(r) / M_vir
        
        if (q<(M_HALO/M_TOTAL))
        {
            s = 10.0*q;
            qq = q*M_HALO/(4*PI*rho0_halo*RS*RS*RS); // qq := q*f(c)
	  
            // solve f(s)-q*f(c)==0
            do {
                f = log(1+s) - s/(1+s) - qq;
                f_ = s / (1+s) / (1+s);
                ds = -0.1 * f / f_;
                
                if (fabs(ds)/s>0.01)
                    ds = s*0.01*ds/fabs(ds);
                s += ds;
            }
            while(fabs(ds/s)>1e-10);
        }
        else {
            /* halo truncation */
            printf("i=%d q=%g\n",i,q); fflush(stdout);
            s = Rvir/RS;
	  
            do {
                f = gam1_halo*gammp(3+a_halo,s)-p1_halo-q*M_TOTAL+Mvir;
                f_ = gam1_halo*exp((a_halo+2)*log(s)-s-gammln(a_halo+3));
                ds = -f/f_;
                s += ds;
            }
            while(fabs(ds/s)>1e-8);
        }
      r = s*RS;
      rt[i] = r;
    }
    
    for (i=1;i<=N;i++)
    {
        rfinal[i]=rt[i];
        mfinal[i]=mt[i];
    }

    spline(rfinal,mt,N,1e40,1e40,m2r);
    spline(mt,rfinal,N,1e40,1e40,r2m);
}


void prepare_cumlative_profile()
{
    int i;

    for (i=2,mfinal[1]=0;i<=N;i++)
        mfinal[i] = (rt[i]/rfinal[i])*mt[i]; //- mass_cumulative_disk(rfinal[i]) - mass_cumulative_bulge(rfinal[i]) - mass_cumulative_gas(rfinal[i]);
    
    for(i=1;i<=(N-1);i++)
        rhofinal[i] = (mfinal[i+1]-mfinal[i]) / (4.0*PI/3) / ( pow(rfinal[i+1],3) - pow(rfinal[i],3) );
    rhofinal[N] = 0;
    
    spline(rfinal,mfinal,N,1e40,1e40,m2r);
    spline(mfinal,rfinal,N,1e40,1e40,r2m);
    spline(rfinal,rhofinal,N,1e40,1e40,rho2r);

    LL = rfinal[N];
}


double gc_int(double x)
{
  return pow(log(1+x)-x/(1+x),0.5)*pow(x,1.5)/pow(1+x,2);
}


double fc(double c)
{
  return c*(0.5-0.5/pow(1+c,2)-log(1+c)/(1+c))/pow(log(1+c)-c/(1+c),2);
}


double gc(double c)
{
  return qromb(gc_int,0,c);
}


double mass_total_dark(double radius)
{
    double x;
    
    if (radius>rfinal[N])
        x = mfinal[N];
    else
        splint(rfinal,mfinal,m2r,N,radius,&x);
    return x;
}


double darkmass_inside_ropt(void)
{
    double ropt=3.2*H;
    return mass_total_dark(ropt);
}


double halo_mass(double r)
{
    double x;
  
    if (r>rfinal[N])
        x = mfinal[N];
    else
        splint(rfinal,mfinal,m2r,N,r,&x);
    
    return x;
}


double halo_q_to_r(double q)
{
    double m,x;
    
    m = mfinal[N]*q;
    splint(mfinal,rfinal,r2m,N,m,&x);
    
    return x;
}


double halo_rho(double r)
{
    double x;

    if (r>rfinal[N])
        x = 0;
    else
        splint(rfinal,rhofinal,rho2r,N,r,&x);

    return x;
}


double comp_Dphi_z_halo(double R, double z)
{
    double r = sqrt(R*R+z*z);
    double M_r = halo_mass(r);
    
    if ((r>0)&&(M_r>0))
        return G*M_r*z/(r*r*r);
    else
        return 0;
}


double comp_Dphi_R_halo(double R,double z)
{
    double r = sqrt(R*R+z*z);
    double M_r = halo_mass(r);
    
    if ((r>0)&&(M_r>0))
        return G*M_r*R/(r*r*r);
    else
        return 0;
}


double comp_rho_halo(double R, double z)
{
    double r = sqrt(R*R+z*z);
    return halo_rho(r);
}


double comp_Dphi_z_BH(double R, double z)
{
    double r = sqrt(R*R+z*z);

    if ((r>0)&&(M_BH>0))
        return G*M_BH*z/(r*r*r);
    else
        return 0;
}


double comp_Dphi_R_BH(double R,double z)
{
    double r = sqrt(R*R+z*z);

    if ((r>0)&&(M_BH>0))
        return G*M_BH*R/(r*r*r);
    else
        return 0;
}
