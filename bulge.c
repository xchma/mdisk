#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "nrsrc/nr.h"
#include "nrsrc/nrutil.h"

#include "prototypes.h"
#include "globvars.h"


static double R,z;


void compute_velocity_dispersions_bulge()
{
    int i,j;
    double z,R,rho;

    if (N_BULGE==0) return;
    
    printf("bulge velocity dispersion field...\n"); fflush(stdout);

    for (i=0; i<=RSIZE; i++)
    {
        printf("bulge A (R,z), %d\n",i);
      
        for (j=0;j<=ZSIZE;j++)
        {
            xl[j+1] = list_z[j];
            yl[j+1] = Dphi_z[i][j]*comp_rho_bulge(list_R[i],list_z[j]);
        }

        spline(xl,yl,ZSIZE+1,1e40,1e40,D2yl);

        for (j=ZSIZE-1,VelDispRz_bulge[i][ZSIZE]=0;j>=0;j--)
        {
            VelDispRz_bulge[i][j] = VelDispRz_bulge[i][j+1];
            if (fabs(yl[j+2])>1e-100 && fabs(yl[j+1])>1e-100)
                VelDispRz_bulge[i][j] += qromb(splint_xl_yl_D2yl,list_z[j],list_z[j+1]);
        }
    }

    for (i=0;i<=RSIZE;i++)
    {
        printf("bulge B (R+dR,z), %d\n",i);

        for(j=0;j<=ZSIZE;j++)
        {
            xl[j+1] = list_z[j];
            yl[j+1] = Dphi_z_dR[i][j]*comp_rho_bulge(list_RplusdR[i],list_z[j]);
        }

        spline(xl,yl,ZSIZE+1,1e40,1e40,D2yl);
      
        for (j=ZSIZE-1,VelDispRz_dR_bulge[i][ZSIZE]=0;j>=0;j--)
        {
            VelDispRz_dR_bulge[i][j] = VelDispRz_dR_bulge[i][j+1];
            if (fabs(yl[j+2])>1e-100 && fabs(yl[j+1])>1e-100)
                VelDispRz_dR_bulge[i][j] += qromb(splint_xl_yl_D2yl,list_z[j],list_z[j+1]);
        }
    }
  
    for (i=0;i<=RSIZE;i++)
    {
        for(j=0;j<=ZSIZE;j++)
        {
            R = list_R[i];
            z = list_z[j];
            rho = comp_rho_bulge(R,z);

            if (rho>0)
            {
                if (i>0)
                    VelDispPhi_bulge[i][j] = (R/rho) * (VelDispRz_dR_bulge[i][j]-VelDispRz_bulge[i][j]) / (list_RplusdR[i]-list_R[i]);
                else
                    VelDispPhi_bulge[i][j] = 0;

                VelDispRz_bulge[i][j] /= rho;
            }
            else
                VelDispRz_bulge[i][j] = VelDispPhi_bulge[i][j] = 0;

            VelVc2_bulge[i][j] = R*Dphi_R[i][j];
            VelDispPhi_bulge[i][j] += VelVc2_bulge[i][j]+VelDispRz_bulge[i][j];
            VelStreamPhi_bulge[i][j] = 0; // no rotation for bulge
            VelDispPhi_bulge[i][j] -= VelStreamPhi_bulge[i][j]*VelStreamPhi_bulge[i][j];

            if (VelDispRz_bulge[i][j]<0)
                VelDispRz_bulge[i][j] = 0;
	  
            if(VelDispPhi_bulge[i][j]<0)
                VelDispPhi_bulge[i][j] = 0;
        }
    }
    printf("done.\n"); fflush(stdout);
}


/* ---------------------------
 * bulge profile and potential
 * --------------------------- */
double comp_rho_bulge(double R, double z)
{
    double r = sqrt(R*R+z*z);
    if (r<1e-6*A) r=1e-6*A;

    switch (BulgeDistribution) {
        case 0: // Hernquist
            return M_BULGE/(2*PI)*A/r/(A+r)/(A+r)/(A+r);
            break;
        case 1: // Exp.
            return M_BULGE/(8*PI*A*A*A) * exp(-r/A);
            break;
    }
}


double mass_cumulative_bulge(double R)
{
    double x = R/BulgeSize;
    switch (BulgeDistribution) {
        case 0: // Hernquist
            return M_BULGE*R*R/(A+R)/(A+R);
            break;
        case 1: // Exp.
            return M_BULGE*(1-(1+x+0.5*x*x)*exp(-x));
            break;
   }
}


double comp_Dphi_z_bulge(double RR,double zz)
{
    double r = sqrt(RR*RR+zz*zz);
    double m = mass_cumulative_bulge(r);

    if ((r>0)&&(m>0))
        return G*m*zz/(r*r*r);
    else
        return 0;
}


double comp_Dphi_R_bulge(double RR,double zz)
{
    double r = sqrt(RR*RR+zz*zz);
    double m = mass_cumulative_bulge(r);

    if ((r>0)&&(m>0))
        return G*m*RR/(r*r*r);
    else
        return 0;
}
